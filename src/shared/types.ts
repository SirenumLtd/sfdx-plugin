export enum PlanFormat {
  JSON = 'json',
  CSV = 'csv'
}

export enum PlanAlignment {
  None = 'none',
  Week = 'week'
}

export interface DataPlanSObject {
  sobject: string,
  filter?: string,
  maxRecordsPerCall?: number,
  fields: string[]
}

export interface DataPlan {
  output?: string,
  input?: string,
  format?: PlanFormat,
  align?: PlanAlignment,
  maxRecordsPerCall?: number,
  sobjects: DataPlanSObject[]
}

export interface PlanMetadata {
  locale: string,
  timezone: string,
  date: string
}

export interface Record {
  attributes : {
    type: string,
    referenceId: string,
    recordTypeId?: string
  },
  Id?: string,
  RecordTypeId? : string
}

export interface UserRecord {
  LocaleSidKey: string,
  TimeZoneSidKey: string
}

