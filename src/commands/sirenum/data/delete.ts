/*
 * Copyright Sirenum (c) 2020.
 */
import { flags } from '@salesforce/command';
import { Messages, SfdxError } from '@salesforce/core';
import { AnyJson } from '@salesforce/ts-types';
import * as fs from 'fs-extra';
import * as path from 'path';

import {InteractiveCommand} from "../../../shared/interactiveCommand";

// Initialize Messages with the current plugin directory
Messages.importMessagesDirectory(__dirname);

// Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = Messages.loadMessages('@sirenum/sfdx-plugin', 'data.delete');

/**
 * Delete records from the org as listed in the plan
 */
export default class Delete extends InteractiveCommand {

  public static description = messages.getMessage('commandDescription');

  public static examples = [
    `$ sfdx sirenum:data:delete my-plan.json`,
    `$ sfdx sirenum:data:delete data/my-plan.json -u my-org-alias`,
    `$ sfdx sirenum:data:delete my-plan.json -i`
  ];

  public static args = [{name: 'plan'}];

  protected static flagsConfig = {
    noprompt: flags.boolean({char: 'p', description: messages.getMessage('noPromptFlagDescription')})
  };

  protected static supportsUsername = true;

  private static getMaxRecordsPerCall(value : any, defaultValue : number) : number {
    return Math.min(Math.max((parseInt(value) || defaultValue), 1), 200);
  }

  public async run(): Promise<AnyJson> {

    const org = await this.selectOrg(messages.getMessage('selectOrg'));

    if (!org) {
      throw new SfdxError(messages.getMessage('errorNoOrg'));
    }

    const planFile = await this.selectFile(messages.getMessage('selectPlan'), '.json', this.args.plan);

    if (!fs.existsSync(planFile)) {
      throw new SfdxError(messages.getMessage('errorNoPlan', [planFile]));
    }

    const plan = fs.readJsonSync(planFile);

    // Confirm the execution
    if (this.flags.interactive || !this.flags.noprompt) {
      const confirmed = await this.ux.confirm(messages.getMessage('deleteConfirmation', [
        path.basename(planFile),
        org.getConnection().getAuthInfoFields().alias || org.getUsername(),
        org.getOrgId()
      ]));

      if (!confirmed) {
        // User says no
        return;
      }
    }

    if (!plan.sobjects || !plan.sobjects.length) {
      throw new SfdxError(messages.getMessage('errorNoSObjects'));
    }

    this.ux.styledHeader(messages.getMessage('deletingPlan', [
      path.basename(planFile),
      org.getConnection().getAuthInfoFields().alias || org.getUsername()
    ]));

    const startTime = Date.now();

    const maxRecordsPerCall = Delete.getMaxRecordsPerCall(plan.maxRecordsPerCall, 200);
    const output = {};

    this.ux.startSpinner(messages.getMessage('statusDeleting'));

    // Iterate in reverse order to delete children before parents
    for (const sobjectItem of plan.sobjects.reverse()) {

      // Ensure the same object type is not imported twice
      if (output.hasOwnProperty(sobjectItem.sobject)) {
        throw new SfdxError(messages.getMessage('errorDuplicateSObjectType', [sobjectItem.sobject]));
      }

      this.ux.setSpinnerStatus(sobjectItem.sobject);

      output[sobjectItem.sobject] = { success: 0, failures: []  };

      // Query the server for records to delete
      const records = await org.getConnection().sobject<Record>(sobjectItem.sobject)
        .select('Id')
        .where(sobjectItem.filter || '')
        .sort('-Id')
        .run();

      // Get the max records per call for this object
      const maxRecordsLimit = Delete.getMaxRecordsPerCall(sobjectItem.maxRecordsPerCall, maxRecordsPerCall);

      // Upload up to the limit in each call
      while (records.length) {
        // Update the status with records to delete
        this.ux.setSpinnerStatus(`${records.length} ${sobjectItem.sobject}`);

        // Get a chunk of record ids to delete
        const idsToDelete = records.splice(0, maxRecordsLimit).map(record => record.Id);

        // Delete the records
        const deletedItems = (await org.getConnection().request({
          method: 'DELETE',
          url: `${org.getConnection().baseUrl()}/composite/sobjects?ids=${idsToDelete.join(',')}`,
        }) as unknown) as DeleteResponse[];

        // Count successes and log errors
        deletedItems.forEach(deletedItem => {
          if (deletedItem.success) {
            output[sobjectItem.sobject].success++;
          } else {
            // Record and log the error
            output[sobjectItem.sobject].failures.push(deletedItem);
            this.ux.error(messages.getMessage('errorDeletingRecord', [
              sobjectItem.sobject, deletedItem.id
            ]))
            deletedItem.errors.forEach(error => {
              this.ux.error(messages.getMessage('errorDeletingRecordDetails', [
                error.statusCode, error.message, error.fields.join()
              ]));
            })
          }
        });
      }

      // Record the progress (left pad the number of records)
      this.ux.log(messages.getMessage('statusProgress', [
        ('    ' + output[sobjectItem.sobject].success).slice(-5),
        sobjectItem.sobject
      ]));
    }

    const seconds = (Date.now() - startTime)/1000;

    this.ux.stopSpinner(messages.getMessage('statusDone', [seconds]));

    // Return an object to be displayed with --json
    return output;
  }
}

/**
 * An SObject record
 */
interface Record {
  Id: string,
  attributes : {
    type: string,
    referenceId: string
  }
}

/**
 * The tree call response
 */
interface DeleteResponse {
  id: string,
  success: boolean,
  errors: {
    statusCode: string,
    message: string,
    fields: string[]
  }[]
}
