/*
 * Copyright Sirenum (c) 2020.
 */
import {flags} from '@salesforce/command';
import {Messages, SfdxError} from '@salesforce/core';
import {AnyJson} from '@salesforce/ts-types';
import {writeToPath} from '@fast-csv/format';
import * as moment from 'moment-timezone';
import * as fs from 'fs-extra';
import * as path from 'path';

import {InteractiveCommand} from "../../../shared/interactiveCommand";
import {DataPlan, DataPlanSObject, PlanFormat, PlanMetadata, Record, UserRecord} from "../../../shared/types";


// Initialize Messages with the current plugin directory
Messages.importMessagesDirectory(__dirname);

// Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = Messages.loadMessages('@sirenum/sfdx-plugin', 'data.export');

// noinspection JSUnusedGlobalSymbols
/**
 * Export records from the org listed on a plan file.
 */
export default class Export extends InteractiveCommand {

  // noinspection JSUnusedGlobalSymbols
  public static description = messages.getMessage('commandDescription');

  // noinspection JSUnusedGlobalSymbols
  public static examples = [
    `$ sfdx sirenum:data:export my-plan.json`,
    `$ sfdx sirenum:data:export data/my-plan.json -d data/set1 -u my-org-alias`,
    `$ sfdx sirenum:data:export my-plan.json -i`
  ];

  // noinspection JSUnusedGlobalSymbols
  public static args = [{name: 'plan'}];

  protected static flagsConfig = {
    outputdir: flags.filepath({char: 'd', description: messages.getMessage('outputFlagDescription')}),
    format: flags.enum<PlanFormat>({
      char: 'f',
      description: messages.getMessage('outputFlagFormat'),
      options: [PlanFormat.JSON, PlanFormat.CSV]
    })
  };

  protected static supportsUsername = true;

  public async run(): Promise<AnyJson> {

    const org = await this.selectOrg(messages.getMessage('selectOrg'));

    if (!org) {
      throw new SfdxError(messages.getMessage('errorNoOrg'));
    }

    const planFile = await this.selectFile(messages.getMessage('selectPlan'), '.json', this.args.plan);

    if (!fs.existsSync(planFile)) {
      throw new SfdxError(messages.getMessage('errorNoPlan', [planFile]));
    }

    const plan: DataPlan = fs.readJsonSync(planFile);

    plan.output = await this.selectFolder(messages.getMessage('selectOutput'),
      path.join(path.dirname(planFile), this.flags.outputdir || plan.output || ''));

    if (!plan.output) {
      throw new SfdxError(messages.getMessage('errorNoOutput'));
    }

    plan.format = (this.flags.format || plan.format || PlanFormat.JSON).toLowerCase();

    if (plan.format !== PlanFormat.JSON && plan.format !== PlanFormat.CSV) {
      throw new SfdxError(messages.getMessage('errorInvalidFormat', [plan.format]));
    }

    if (this.flags.interactive) {
      const confirm = await this.ux.log('').confirm(messages.getMessage('exportConfirmation', [
        path.basename(planFile),
        org.getConnection().getAuthInfoFields().alias || org.getUsername(),
        plan.output
      ]));

      if (!confirm) {
        return;
      }
    }

    if (!plan.sobjects || !plan.sobjects.length) {
      throw new SfdxError(messages.getMessage('errorNoSObjects'));
    }

    this.ux.styledHeader(messages.getMessage('exportingPlan', [
      path.basename(planFile),
      org.getConnection().getAuthInfoFields().alias || org.getUsername(),
      plan.output
    ]));

    const startTime = Date.now();

    const idLookup = {};
    const output = {};

    this.ux.startSpinner(messages.getMessage('statusExporting'));

    fs.ensureDirSync(plan.output);

    // Get user info for writing metadata and date/time in the correct timezone (when exporting to CSV)
    const userInfo = (await org.getConnection().sobject<UserRecord>('User')
      .select(['LocaleSidKey', 'TimeZoneSidKey'])
      .where('Username=\'' + org.getUsername() + '\'')
      .run())[0];

    // Write metadata
    const metadata: PlanMetadata = {
      locale: userInfo.LocaleSidKey.replace('_', '-'),
      timezone: userInfo.TimeZoneSidKey,
      date: moment().tz(userInfo.TimeZoneSidKey).format('YYYY-MM-DD')
    }
    fs.outputJsonSync(path.join(plan.output, '_metadata.json'), metadata, {spaces: 2});

    // Export all objects
    for (const sobjectItem of plan.sobjects) {

      // Ensure the same object type is not exported twice
      if (output.hasOwnProperty(sobjectItem.sobject)) {
        throw new SfdxError(messages.getMessage('errorDuplicateSObjectType', [sobjectItem.sobject]));
      }

      this.ux.setSpinnerStatus(sobjectItem.sobject);

      // Get the object describe
      const describe = await org.getConnection().describe(sobjectItem.sobject);
      const fieldsByName = describe.fields.reduce((map, field) => (map[field.name] = field) && map, {});
      const recordTypeApiNamesById = describe.recordTypeInfos.reduce(
        (map, recordType) => (map[recordType.recordTypeId] = recordType.developerName) && map, {});

      const fields = [].concat(sobjectItem.fields);

      // Ensure the ID field is queried
      if (fields.every(field => field.toLowerCase() !== 'id')) {
        fields.push('Id');
      }

      // Ensure the RecordTypeId is queried
      if (fieldsByName['RecordTypeId'] && fields.every(field => field.toLowerCase() !== 'recordtypeid')) {
        fields.push('RecordTypeId');
      }

      // Query the server
      const records = await org.getConnection().sobject<Record>(sobjectItem.sobject)
        .select(fields)
        .where(sobjectItem.filter || '')
        .sort('Id')
        .run();

      // Replace object IDs with references and record type IDs with API names
      records.forEach((record, index) => {
        record.attributes = {
          type: sobjectItem.sobject,
          referenceId: `${sobjectItem.sobject}Ref${index + 1}`
        };

        if (fieldsByName['RecordTypeId']) {
          record.attributes.recordTypeId = recordTypeApiNamesById[record.RecordTypeId];

          if (!sobjectItem.fields.includes('RecordTypeId')) {
            delete record.RecordTypeId;
          }
        }

        idLookup[record.Id] = record.attributes.referenceId;

        if (!sobjectItem.fields.includes('Id')) {
          delete record.Id
        }
      });

      // Replace lookup ids with references and record type IDs with API name
      records.forEach(record => {
        for (const key of Object.keys(record)) {
          const value = record[key];

          if (typeof value === 'string' && idLookup[value]) {
            record[key] = '@' + idLookup[value];
          }
        }
      });

      // Save the data
      await this.saveRecords(sobjectItem, records, plan, fieldsByName, metadata);

      // Set the output
      output[sobjectItem.sobject] = records.length;

      // Record the progress (left pad the number of records)
      this.ux.log(messages.getMessage('statusProgress', [
        ('    ' + records.length).slice(-5),
        sobjectItem.sobject
      ]));
    }

    const seconds = (Date.now() - startTime)/1000;

    this.ux.stopSpinner(messages.getMessage('statusDone', [seconds]));

    // Return an object to be displayed with --json
    return output;
  }

  /**
   * Save the records into a file in the specified file format
   *
   * @param sobjectItem the plan sobject item
   * @param records the records to save
   * @param plan the export plan
   * @param fieldsByName map of field infos by field API name
   * @param metadata the plan metadata with the user locale and timezone
   */
  private async saveRecords(sobjectItem: DataPlanSObject,
                         records: Partial<Record>[],
                         plan: DataPlan,
                         fieldsByName: {},
                         metadata: PlanMetadata) {
    const file = path.join(plan.output, `${sobjectItem.sobject}.${plan.format}`);

    if (plan.format === PlanFormat.JSON) {
      await fs.outputJson(file, records, {spaces: 2});
    }

    if (plan.format === PlanFormat.CSV) {
      moment.locale(metadata.locale);

      const headers = ['referenceId'];

      if (fieldsByName['RecordTypeId']) {
        headers.push('recordTypeId');
      }

      headers.push(...sobjectItem.fields);

      await writeToPath(file, records, {
        writeBOM: true,
        headers: headers,
        transform: (record : Record) => {
          const row = {
            referenceId: record.attributes.referenceId,
            recordTypeId: record.attributes.recordTypeId
          };

          sobjectItem.fields.forEach(fieldName => {
            let fieldValue = record[fieldName];

            // Write Date, Time and Datetime in locale readable format
            switch (fieldValue && fieldsByName[fieldName] && fieldsByName[fieldName].type) {
              case 'date':
                fieldValue = moment(fieldValue).format('ll');
                break;

              case 'time':
                fieldValue = moment('2000-01-01T' + fieldValue).format('LT');
                break;

              case 'datetime':
                fieldValue = moment.utc(fieldValue).tz(metadata.timezone).format('lll');
                break;
            }

            row[fieldName] = fieldValue
          });

          return row;
        }
      });
    }
  }
}

