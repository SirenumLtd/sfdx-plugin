@sirenum/sfdx-plugin
====================

A plugin for SFDX

# Installing the plugin
Update SFDX to the latest version and install the plugin using the following terminal command:
```
$ sfdx plugins:install https://bitbucket.org/SirenumLtd/sfdx-plugin.git
```
# The Plan file

All `sirenum:data` commands use a plan file to describe what to perform.
The plan files is a JSON file with the following:

| Property | Values            | Description             |
|----------|-------------------|-------------------------|
| output   | String (optional) | Export folder path relative to the plan file. |
| input    | String (optional) | Import folder path relative to the plan file. |
| format   | 'json' or 'csv' (optional) | The data format for export or import. Default to `json`. |
| align    | 'none' or 'week' (optional) | Allow to align the imported data to the current week using the export date as specified in the `_metadata.json` file in the `inout` folder. Default to `none`. |
| maxRecordsPerCall | Integer (optional) | The maximum number of records to import in one call. Default to 200 and limited to 200. |
| sobjects | SObjectInfo Array | List the objects to operate on, each item is defined below. |

SObjectInfo structure:

| Property | Values            | Description             |
|----------|-------------------|-------------------------|
| sobject  | String            | The SObject full API name. |
| filter   | String (optional) | SOQL WHERE clause to filter the objects to export or delete. |
| maxRecordsPerCall | Integer (optional) | The maximum number of records to import in one call. Default the plan's value and limited to 200. |
| fields   | String Array      | List of the field to export or import, must be the full API name and dues not support field paths. Lookups must specify the ID field.

Example plan for Accounts and Contacts
```json
{
  "output": "data-set",
  "input": "data-set",
  "format": "csv",
  "align": "week",
  "sobjects": [
    {
      "sobject": "Account",
      "fields" : [
        "Name",
        "Industry"
      ]
    },
    {
      "sobject": "Contact",
      "fields" : [
        "AccountId",
        "FirstName",
        "LastName",
        "Phone"
      ]
    } 
  ]
}
```

* Exporting to CSV format exports Dates and Times in the locale and timezone of the user that performs the export.
* Importing CSV files uses the locale and timezone specified in the `_metadata.json` file or fall back to the current user that performs the import.
* Importing data with alignment set to 'none' or 'week' adjust the Datetime values to the timezone of the current user that performs the import.
* Importing data with alignment set to 'week' adds the number of weeks between today to the export date found in the `_metadata.json` file.
* **Importing data always create new records.**

# Commands
<!-- commands -->
* [`sfdx sirenum:data:delete [-p] [-i] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-sirenumdatadelete--p--i--u-string---apiversion-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx sirenum:data:export [-d <filepath>] [-f json|csv] [-i] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-sirenumdataexport--d-filepath--f-jsoncsv--i--u-string---apiversion-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)
* [`sfdx sirenum:data:import [-p <filepath>] [-d <filepath>] [-f json|csv] [-a none|week] [-i] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`](#sfdx-sirenumdataimport--p-filepath--d-filepath--f-jsoncsv--a-noneweek--i--u-string---apiversion-string---json---loglevel-tracedebuginfowarnerrorfataltracedebuginfowarnerrorfatal)

## `sfdx sirenum:data:delete [-p] [-i] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

Deletes data specified in a plan from an org

```
USAGE
  $ sfdx sirenum:data:delete [-p] [-i] [-u <string>] [--apiversion <string>] [--json] [--loglevel 
  trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -i, --interactive                                                                 Interactive input
  -p, --noprompt                                                                    No prompt to confirm deletion

  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  $ sfdx sirenum:data:delete my-plan.json
  $ sfdx sirenum:data:delete data/my-plan.json -u my-org-alias
  $ sfdx sirenum:data:delete my-plan.json -i
```

## `sfdx sirenum:data:export [-d <filepath>] [-f json|csv] [-i] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

Exports data specified in a plan from an org to local files

```
USAGE
  $ sfdx sirenum:data:export [-d <filepath>] [-f json|csv] [-i] [-u <string>] [--apiversion <string>] [--json] 
  [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -d, --outputdir=outputdir                                                         Overrides the plan's output
                                                                                    directory

  -f, --format=(json|csv)                                                           Overrides the plan's format

  -i, --interactive                                                                 Interactive input

  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  $ sfdx sirenum:data:export my-plan.json
  $ sfdx sirenum:data:export data/my-plan.json -d data/set1 -u my-org-alias
  $ sfdx sirenum:data:export my-plan.json -i
```

## `sfdx sirenum:data:import [-p <filepath>] [-d <filepath>] [-f json|csv] [-a none|week] [-i] [-u <string>] [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]`

Imports data specified in a plan from local files into an org

```
USAGE
  $ sfdx sirenum:data:import [-p <filepath>] [-d <filepath>] [-f json|csv] [-a none|week] [-i] [-u <string>] 
  [--apiversion <string>] [--json] [--loglevel trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL]

OPTIONS
  -a, --align=(none|week)                                                           Overrides the plan's alignment
  -d, --inputdir=inputdir                                                           Overrides the plan input directory
  -f, --format=(json|csv)                                                           Overrides the plan's format
  -i, --interactive                                                                 Interactive input
  -p, --plan=plan                                                                   Select the plan file to use

  -u, --targetusername=targetusername                                               username or alias for the target
                                                                                    org; overrides default target org

  --apiversion=apiversion                                                           override the api version used for
                                                                                    api requests made by this command

  --json                                                                            format output as json

  --loglevel=(trace|debug|info|warn|error|fatal|TRACE|DEBUG|INFO|WARN|ERROR|FATAL)  [default: warn] logging level for
                                                                                    this command invocation

EXAMPLES
  $ sfdx sirenum:data:import my-plan.json
  $ sfdx sirenum:data:import data/my-plan.json -d data/set1 -u my-org-alias
  $ sfdx sirenum:data:import my-plan.json -i
```
<!-- commandsstop -->

# Developing the plugin

Ensure the following prerequisites as listed in [Salesforce documentation](https://developer.salesforce.com/docs/atlas.en-us.sfdx_cli_plugins.meta/sfdx_cli_plugins/cli_plugins_generate_prepare.htm)

1.  Install or update Node.js to version 8.0.0 or higher.
2.  Install the Yarn package manager: `$ npm install -g yarn`
3.  Install TypeScript target es2017: `$ npm install -g typescript`
4.  Install or update Salesforce sfdx CLI: `$ sfdx update`

Link the plugin to sfdx using `$ sfdx plugins:link` at the root of the project. Note that once it is linked you will only be able to run the tool from within the root of the project.

To test the plugin it is possible to install it from a remote branch using `sfdx plugins:install https://bitbucket.org/SirenumLtd/sfdx-plugin.git#<BRANCH_NAME>`

Use `sfdx plugins:uninstall @sirenum/sfdx-plugin` to uninstall the plugin in-between testing otherwise it might not update
