import { expect, test } from '@salesforce/command/lib/test';
// import { ensureJsonMap, ensureString } from '@salesforce/ts-types';

describe('sirenum:export', () => {
  test
    .withOrg({ username: 'test@org.com' }, true)
    .stderr()
    .command(['sirenum:export', 'some-plan.json', '--targetusername', 'test@org.com'])
    .it('fail when no plan', ctx => {
      expect(ctx.stderr).to.contain('No plan found');
    });

  // test
  //   .withOrg({ username: 'test@org.com' }, true)
  //   .withConnectionRequest(request => {
  //     const requestMap = ensureJsonMap(request);
  //     if (ensureString(requestMap.url).match(/Organization/)) {
  //       return Promise.resolve({ records: [ { Name: 'Super Awesome Org', TrialExpirationDate: '2018-03-20T23:24:11.000+0000'}] });
  //     }
  //     return Promise.resolve({ records: [] });
  //   })
  //   .stdout()
  //   .command(['sirenum:export', '--targetusername', 'test@org.com'])
  //   .it('runs sirenum:export', ctx => {
  //     expect(ctx.stdout).to.contain('Hello world! This is org: Super Awesome Org and I will be around until Tue Mar 20 2018!');
  //   });
});
