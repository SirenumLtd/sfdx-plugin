"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/*
 * Copyright Sirenum (c) 2020.
 */
const command_1 = require("@salesforce/command");
const core_1 = require("@salesforce/core");
const format_1 = require("@fast-csv/format");
const moment = require("moment-timezone");
const fs = require("fs-extra");
const path = require("path");
const interactiveCommand_1 = require("../../../shared/interactiveCommand");
const types_1 = require("../../../shared/types");
// Initialize Messages with the current plugin directory
core_1.Messages.importMessagesDirectory(__dirname);
// Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = core_1.Messages.loadMessages('@sirenum/sfdx-plugin', 'data.export');
// noinspection JSUnusedGlobalSymbols
/**
 * Export records from the org listed on a plan file.
 */
class Export extends interactiveCommand_1.InteractiveCommand {
    async run() {
        const org = await this.selectOrg(messages.getMessage('selectOrg'));
        if (!org) {
            throw new core_1.SfdxError(messages.getMessage('errorNoOrg'));
        }
        const planFile = await this.selectFile(messages.getMessage('selectPlan'), '.json', this.args.plan);
        if (!fs.existsSync(planFile)) {
            throw new core_1.SfdxError(messages.getMessage('errorNoPlan', [planFile]));
        }
        const plan = fs.readJsonSync(planFile);
        plan.output = await this.selectFolder(messages.getMessage('selectOutput'), path.join(path.dirname(planFile), this.flags.outputdir || plan.output || ''));
        if (!plan.output) {
            throw new core_1.SfdxError(messages.getMessage('errorNoOutput'));
        }
        plan.format = (this.flags.format || plan.format || types_1.PlanFormat.JSON).toLowerCase();
        if (plan.format !== types_1.PlanFormat.JSON && plan.format !== types_1.PlanFormat.CSV) {
            throw new core_1.SfdxError(messages.getMessage('errorInvalidFormat', [plan.format]));
        }
        if (this.flags.interactive) {
            const confirm = await this.ux.log('').confirm(messages.getMessage('exportConfirmation', [
                path.basename(planFile),
                org.getConnection().getAuthInfoFields().alias || org.getUsername(),
                plan.output
            ]));
            if (!confirm) {
                return;
            }
        }
        if (!plan.sobjects || !plan.sobjects.length) {
            throw new core_1.SfdxError(messages.getMessage('errorNoSObjects'));
        }
        this.ux.styledHeader(messages.getMessage('exportingPlan', [
            path.basename(planFile),
            org.getConnection().getAuthInfoFields().alias || org.getUsername(),
            plan.output
        ]));
        const startTime = Date.now();
        const idLookup = {};
        const output = {};
        this.ux.startSpinner(messages.getMessage('statusExporting'));
        fs.ensureDirSync(plan.output);
        // Get user info for writing metadata and date/time in the correct timezone (when exporting to CSV)
        const userInfo = (await org.getConnection().sobject('User')
            .select(['LocaleSidKey', 'TimeZoneSidKey'])
            .where('Username=\'' + org.getUsername() + '\'')
            .run())[0];
        // Write metadata
        const metadata = {
            locale: userInfo.LocaleSidKey.replace('_', '-'),
            timezone: userInfo.TimeZoneSidKey,
            date: moment().tz(userInfo.TimeZoneSidKey).format('YYYY-MM-DD')
        };
        fs.outputJsonSync(path.join(plan.output, '_metadata.json'), metadata, { spaces: 2 });
        // Export all objects
        for (const sobjectItem of plan.sobjects) {
            // Ensure the same object type is not exported twice
            if (output.hasOwnProperty(sobjectItem.sobject)) {
                throw new core_1.SfdxError(messages.getMessage('errorDuplicateSObjectType', [sobjectItem.sobject]));
            }
            this.ux.setSpinnerStatus(sobjectItem.sobject);
            // Get the object describe
            const describe = await org.getConnection().describe(sobjectItem.sobject);
            const fieldsByName = describe.fields.reduce((map, field) => (map[field.name] = field) && map, {});
            const recordTypeApiNamesById = describe.recordTypeInfos.reduce((map, recordType) => (map[recordType.recordTypeId] = recordType.developerName) && map, {});
            const fields = [].concat(sobjectItem.fields);
            // Ensure the ID field is queried
            if (fields.every(field => field.toLowerCase() !== 'id')) {
                fields.push('Id');
            }
            // Ensure the RecordTypeId is queried
            if (fieldsByName['RecordTypeId'] && fields.every(field => field.toLowerCase() !== 'recordtypeid')) {
                fields.push('RecordTypeId');
            }
            // Query the server
            const records = await org.getConnection().sobject(sobjectItem.sobject)
                .select(fields)
                .where(sobjectItem.filter || '')
                .sort('Id')
                .run();
            // Replace object IDs with references and record type IDs with API names
            records.forEach((record, index) => {
                record.attributes = {
                    type: sobjectItem.sobject,
                    referenceId: `${sobjectItem.sobject}Ref${index + 1}`
                };
                if (fieldsByName['RecordTypeId']) {
                    record.attributes.recordTypeId = recordTypeApiNamesById[record.RecordTypeId];
                    if (!sobjectItem.fields.includes('RecordTypeId')) {
                        delete record.RecordTypeId;
                    }
                }
                idLookup[record.Id] = record.attributes.referenceId;
                if (!sobjectItem.fields.includes('Id')) {
                    delete record.Id;
                }
            });
            // Replace lookup ids with references and record type IDs with API name
            records.forEach(record => {
                for (const key of Object.keys(record)) {
                    const value = record[key];
                    if (typeof value === 'string' && idLookup[value]) {
                        record[key] = '@' + idLookup[value];
                    }
                }
            });
            // Save the data
            await this.saveRecords(sobjectItem, records, plan, fieldsByName, metadata);
            // Set the output
            output[sobjectItem.sobject] = records.length;
            // Record the progress (left pad the number of records)
            this.ux.log(messages.getMessage('statusProgress', [
                ('    ' + records.length).slice(-5),
                sobjectItem.sobject
            ]));
        }
        const seconds = (Date.now() - startTime) / 1000;
        this.ux.stopSpinner(messages.getMessage('statusDone', [seconds]));
        // Return an object to be displayed with --json
        return output;
    }
    /**
     * Save the records into a file in the specified file format
     *
     * @param sobjectItem the plan sobject item
     * @param records the records to save
     * @param plan the export plan
     * @param fieldsByName map of field infos by field API name
     * @param metadata the plan metadata with the user locale and timezone
     */
    async saveRecords(sobjectItem, records, plan, fieldsByName, metadata) {
        const file = path.join(plan.output, `${sobjectItem.sobject}.${plan.format}`);
        if (plan.format === types_1.PlanFormat.JSON) {
            await fs.outputJson(file, records, { spaces: 2 });
        }
        if (plan.format === types_1.PlanFormat.CSV) {
            moment.locale(metadata.locale);
            const headers = ['referenceId'];
            if (fieldsByName['RecordTypeId']) {
                headers.push('recordTypeId');
            }
            headers.push(...sobjectItem.fields);
            await format_1.writeToPath(file, records, {
                writeBOM: true,
                headers: headers,
                transform: (record) => {
                    const row = {
                        referenceId: record.attributes.referenceId,
                        recordTypeId: record.attributes.recordTypeId
                    };
                    sobjectItem.fields.forEach(fieldName => {
                        let fieldValue = record[fieldName];
                        // Write Date, Time and Datetime in locale readable format
                        switch (fieldValue && fieldsByName[fieldName] && fieldsByName[fieldName].type) {
                            case 'date':
                                fieldValue = moment(fieldValue).format('ll');
                                break;
                            case 'time':
                                fieldValue = moment('2000-01-01T' + fieldValue).format('LT');
                                break;
                            case 'datetime':
                                fieldValue = moment.utc(fieldValue).tz(metadata.timezone).format('lll');
                                break;
                        }
                        row[fieldName] = fieldValue;
                    });
                    return row;
                }
            });
        }
    }
}
exports.default = Export;
// noinspection JSUnusedGlobalSymbols
Export.description = messages.getMessage('commandDescription');
// noinspection JSUnusedGlobalSymbols
Export.examples = [
    `$ sfdx sirenum:data:export my-plan.json`,
    `$ sfdx sirenum:data:export data/my-plan.json -d data/set1 -u my-org-alias`,
    `$ sfdx sirenum:data:export my-plan.json -i`
];
// noinspection JSUnusedGlobalSymbols
Export.args = [{ name: 'plan' }];
Export.flagsConfig = {
    outputdir: command_1.flags.filepath({ char: 'd', description: messages.getMessage('outputFlagDescription') }),
    format: command_1.flags.enum({
        char: 'f',
        description: messages.getMessage('outputFlagFormat'),
        options: [types_1.PlanFormat.JSON, types_1.PlanFormat.CSV]
    })
};
Export.supportsUsername = true;
//# sourceMappingURL=export.js.map