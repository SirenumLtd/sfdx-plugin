"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/*
 * Copyright Sirenum (c) 2020.
 */
const command_1 = require("@salesforce/command");
const core_1 = require("@salesforce/core");
const fs = require("fs-extra");
const path = require("path");
const interactiveCommand_1 = require("../../../shared/interactiveCommand");
// Initialize Messages with the current plugin directory
core_1.Messages.importMessagesDirectory(__dirname);
// Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = core_1.Messages.loadMessages('@sirenum/sfdx-plugin', 'data.delete');
/**
 * Delete records from the org as listed in the plan
 */
class Delete extends interactiveCommand_1.InteractiveCommand {
    static getMaxRecordsPerCall(value, defaultValue) {
        return Math.min(Math.max((parseInt(value) || defaultValue), 1), 200);
    }
    async run() {
        const org = await this.selectOrg(messages.getMessage('selectOrg'));
        if (!org) {
            throw new core_1.SfdxError(messages.getMessage('errorNoOrg'));
        }
        const planFile = await this.selectFile(messages.getMessage('selectPlan'), '.json', this.args.plan);
        if (!fs.existsSync(planFile)) {
            throw new core_1.SfdxError(messages.getMessage('errorNoPlan', [planFile]));
        }
        const plan = fs.readJsonSync(planFile);
        // Confirm the execution
        if (this.flags.interactive || !this.flags.noprompt) {
            const confirmed = await this.ux.confirm(messages.getMessage('deleteConfirmation', [
                path.basename(planFile),
                org.getConnection().getAuthInfoFields().alias || org.getUsername(),
                org.getOrgId()
            ]));
            if (!confirmed) {
                // User says no
                return;
            }
        }
        if (!plan.sobjects || !plan.sobjects.length) {
            throw new core_1.SfdxError(messages.getMessage('errorNoSObjects'));
        }
        this.ux.styledHeader(messages.getMessage('deletingPlan', [
            path.basename(planFile),
            org.getConnection().getAuthInfoFields().alias || org.getUsername()
        ]));
        const startTime = Date.now();
        const maxRecordsPerCall = Delete.getMaxRecordsPerCall(plan.maxRecordsPerCall, 200);
        const output = {};
        this.ux.startSpinner(messages.getMessage('statusDeleting'));
        // Iterate in reverse order to delete children before parents
        for (const sobjectItem of plan.sobjects.reverse()) {
            // Ensure the same object type is not imported twice
            if (output.hasOwnProperty(sobjectItem.sobject)) {
                throw new core_1.SfdxError(messages.getMessage('errorDuplicateSObjectType', [sobjectItem.sobject]));
            }
            this.ux.setSpinnerStatus(sobjectItem.sobject);
            output[sobjectItem.sobject] = { success: 0, failures: [] };
            // Query the server for records to delete
            const records = await org.getConnection().sobject(sobjectItem.sobject)
                .select('Id')
                .where(sobjectItem.filter || '')
                .sort('-Id')
                .run();
            // Get the max records per call for this object
            const maxRecordsLimit = Delete.getMaxRecordsPerCall(sobjectItem.maxRecordsPerCall, maxRecordsPerCall);
            // Upload up to the limit in each call
            while (records.length) {
                // Update the status with records to delete
                this.ux.setSpinnerStatus(`${records.length} ${sobjectItem.sobject}`);
                // Get a chunk of record ids to delete
                const idsToDelete = records.splice(0, maxRecordsLimit).map(record => record.Id);
                // Delete the records
                const deletedItems = await org.getConnection().request({
                    method: 'DELETE',
                    url: `${org.getConnection().baseUrl()}/composite/sobjects?ids=${idsToDelete.join(',')}`,
                });
                // Count successes and log errors
                deletedItems.forEach(deletedItem => {
                    if (deletedItem.success) {
                        output[sobjectItem.sobject].success++;
                    }
                    else {
                        // Record and log the error
                        output[sobjectItem.sobject].failures.push(deletedItem);
                        this.ux.error(messages.getMessage('errorDeletingRecord', [
                            sobjectItem.sobject, deletedItem.id
                        ]));
                        deletedItem.errors.forEach(error => {
                            this.ux.error(messages.getMessage('errorDeletingRecordDetails', [
                                error.statusCode, error.message, error.fields.join()
                            ]));
                        });
                    }
                });
            }
            // Record the progress (left pad the number of records)
            this.ux.log(messages.getMessage('statusProgress', [
                ('    ' + output[sobjectItem.sobject].success).slice(-5),
                sobjectItem.sobject
            ]));
        }
        const seconds = (Date.now() - startTime) / 1000;
        this.ux.stopSpinner(messages.getMessage('statusDone', [seconds]));
        // Return an object to be displayed with --json
        return output;
    }
}
exports.default = Delete;
Delete.description = messages.getMessage('commandDescription');
Delete.examples = [
    `$ sfdx sirenum:data:delete my-plan.json`,
    `$ sfdx sirenum:data:delete data/my-plan.json -u my-org-alias`,
    `$ sfdx sirenum:data:delete my-plan.json -i`
];
Delete.args = [{ name: 'plan' }];
Delete.flagsConfig = {
    noprompt: command_1.flags.boolean({ char: 'p', description: messages.getMessage('noPromptFlagDescription') })
};
Delete.supportsUsername = true;
//# sourceMappingURL=delete.js.map