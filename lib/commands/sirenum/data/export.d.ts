import { flags } from '@salesforce/command';
import { AnyJson } from '@salesforce/ts-types';
import { InteractiveCommand } from "../../../shared/interactiveCommand";
import { PlanFormat } from "../../../shared/types";
/**
 * Export records from the org listed on a plan file.
 */
export default class Export extends InteractiveCommand {
    static description: string;
    static examples: string[];
    static args: {
        name: string;
    }[];
    protected static flagsConfig: {
        outputdir: flags.Discriminated<flags.Option<string>>;
        format: flags.Discriminated<flags.Enum<PlanFormat>>;
    };
    protected static supportsUsername: boolean;
    run(): Promise<AnyJson>;
    /**
     * Save the records into a file in the specified file format
     *
     * @param sobjectItem the plan sobject item
     * @param records the records to save
     * @param plan the export plan
     * @param fieldsByName map of field infos by field API name
     * @param metadata the plan metadata with the user locale and timezone
     */
    private saveRecords;
}
