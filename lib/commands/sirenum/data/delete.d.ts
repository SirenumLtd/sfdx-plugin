import { flags } from '@salesforce/command';
import { AnyJson } from '@salesforce/ts-types';
import { InteractiveCommand } from "../../../shared/interactiveCommand";
/**
 * Delete records from the org as listed in the plan
 */
export default class Delete extends InteractiveCommand {
    static description: string;
    static examples: string[];
    static args: {
        name: string;
    }[];
    protected static flagsConfig: {
        noprompt: flags.Discriminated<flags.Boolean<boolean>>;
    };
    protected static supportsUsername: boolean;
    private static getMaxRecordsPerCall;
    run(): Promise<AnyJson>;
}
