"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/*
 * Copyright Sirenum (c) 2020.
 */
const command_1 = require("@salesforce/command");
const core_1 = require("@salesforce/core");
const parse_1 = require("@fast-csv/parse");
const fs = require("fs-extra");
const path = require("path");
const interactiveCommand_1 = require("../../../shared/interactiveCommand");
const types_1 = require("../../../shared/types");
const moment = require("moment-timezone");
// Initialize Messages with the current plugin directory
core_1.Messages.importMessagesDirectory(__dirname);
// Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = core_1.Messages.loadMessages('@sirenum/sfdx-plugin', 'data.import');
// noinspection JSUnusedGlobalSymbols
/**
 * Import records into an org as listed in the plan
 */
class Import extends interactiveCommand_1.InteractiveCommand {
    static getMaxRecordsPerCall(value, defaultValue) {
        return Math.min(Math.max((parseInt(value) || defaultValue), 1), 200);
    }
    async run() {
        var _a;
        const org = await this.selectOrg(messages.getMessage('selectOrg'));
        if (!org) {
            throw new core_1.SfdxError(messages.getMessage('errorNoOrg'));
        }
        if (this.flags.plan && this.args.plan) {
            // Throw an error if the user tried setting the plan using both the arg and the plan flag.
            throw new core_1.SfdxError(messages.getMessage('errorSpecifyOnePlan'));
        }
        else if (this.args.plan) {
            // Display a warning when using the plan arg as this is considered deprecated and replaced by the plan flag.
            console.warn(messages.getMessage('warningPlanArgDeprecated'));
        }
        const planFile = await this.selectFile(messages.getMessage('selectPlan'), '.json', (_a = this.flags.plan) !== null && _a !== void 0 ? _a : this.args.plan);
        if (!fs.existsSync(planFile)) {
            throw new core_1.SfdxError(messages.getMessage('errorNoPlan', [planFile]));
        }
        const plan = fs.readJsonSync(planFile);
        plan.input = await this.selectFolder(messages.getMessage('selectInput'), path.join(path.dirname(planFile), this.flags.inputdir || plan.input || ''));
        if (!fs.pathExistsSync(plan.input)) {
            throw new core_1.SfdxError(messages.getMessage('errorNoInput', [plan.input]));
        }
        plan.format = (this.flags.format || plan.format || types_1.PlanFormat.JSON).toLowerCase();
        if (plan.format !== types_1.PlanFormat.JSON && plan.format !== types_1.PlanFormat.CSV) {
            throw new core_1.SfdxError(messages.getMessage('errorInvalidFormat', [plan.format]));
        }
        if (this.flags.interactive) {
            const confirm = await this.ux.log('').confirm(messages.getMessage('importConfirmation', [
                path.basename(planFile),
                plan.input,
                org.getConnection().getAuthInfoFields().alias || org.getUsername()
            ]));
            if (!confirm) {
                return;
            }
        }
        if (!plan.sobjects || !plan.sobjects.length) {
            throw new core_1.SfdxError(messages.getMessage('errorNoSObjects'));
        }
        this.ux.styledHeader(messages.getMessage('importingPlan', [
            path.basename(planFile),
            plan.input,
            org.getConnection().getAuthInfoFields().alias || org.getUsername()
        ]));
        const startTime = Date.now();
        const maxRecordsPerCall = Import.getMaxRecordsPerCall(plan.maxRecordsPerCall, 200);
        const idLookup = {};
        const output = {};
        this.ux.startSpinner(messages.getMessage('statusImporting'));
        // Get user info for adjusting time to the the correct timezone (when aligning the plan)
        const userInfo = (await org.getConnection().sobject('User')
            .select(['LocaleSidKey', 'TimeZoneSidKey'])
            .where('Username=\'' + org.getUsername() + '\'')
            .run())[0];
        // Read metadata or (default to the user's details)
        const metadataFile = path.join(plan.input, '_metadata.json');
        const metadata = fs.existsSync(metadataFile) ? fs.readJSONSync(metadataFile) : {};
        metadata.locale = metadata.locale || userInfo.LocaleSidKey;
        metadata.timezone = metadata.timezone || userInfo.TimeZoneSidKey;
        metadata.date = metadata.date || moment().tz(metadata.timezone).format('YYYY-MM-DD');
        // Sanitize the plan alignment: 'week' or undefined
        plan.align = (this.flags.align || plan.align || '').toLowerCase() === types_1.PlanAlignment.Week ? types_1.PlanAlignment.Week : undefined;
        // Calculate days to add (full weeks)
        const daysToAdd = 7 * Math.floor((plan.align !== types_1.PlanAlignment.Week ? 0 :
            moment.tz(moment(), metadata.timezone).diff(moment.tz(metadata.date, metadata.timezone), 'd')) / 7);
        for (const sobjectItem of plan.sobjects) {
            // Ensure the same object type is not imported twice
            if (output.hasOwnProperty(sobjectItem.sobject)) {
                throw new core_1.SfdxError(messages.getMessage('errorDuplicateSObjectType', [sobjectItem.sobject]));
            }
            this.ux.setSpinnerStatus(sobjectItem.sobject);
            output[sobjectItem.sobject] = { success: 0, results: [] };
            // Get the object describe
            const describe = await org.getConnection().describe(sobjectItem.sobject);
            const fieldsByName = describe.fields.reduce((map, field) => (map[field.name] = field) && map, {});
            const recordTypeIdsByApiName = describe.recordTypeInfos.reduce((map, recordType) => (map[recordType.developerName] = recordType.recordTypeId) && map, {});
            // Read the file
            const records = await this.loadRecords(sobjectItem, plan, fieldsByName, metadata);
            // Get the max records per call for this object
            const maxRecordsLimit = Import.getMaxRecordsPerCall(sobjectItem.maxRecordsPerCall, maxRecordsPerCall);
            // Upload up to the limit in each call
            while (records.length) {
                // Update the status with records to import
                this.ux.setSpinnerStatus(`${records.length} ${sobjectItem.sobject}`);
                // Get a chunk of records to process
                const recordsToProcess = records.splice(0, maxRecordsLimit);
                // Resolve record types and date/time alignment
                recordsToProcess.forEach(record => {
                    for (const fieldName of Object.keys(record)) {
                        const fieldValue = record[fieldName];
                        // Resolve the record type
                        if (record.attributes.recordTypeId) {
                            record.RecordTypeId = recordTypeIdsByApiName[record.attributes.recordTypeId];
                        }
                        // Align Date and Datetime values
                        if (plan.align && fieldValue !== null && fieldsByName[fieldName]) {
                            switch (fieldsByName[fieldName].type) {
                                case 'date':
                                    // Read the data in the destination timezone
                                    record[fieldName] = moment.tz(fieldValue, userInfo.TimeZoneSidKey).add(daysToAdd, 'd').format('YYYY-MM-DD');
                                    break;
                                case 'datetime':
                                    // Read in one timezone and switch to the destination timezone while retaining the date and time-of-day
                                    record[fieldName] = moment.tz(fieldValue, metadata.timezone).tz(userInfo.TimeZoneSidKey, true).add(daysToAdd, 'd').toISOString();
                                    break;
                            }
                        }
                    }
                });
                // Insert records while resolving references
                while (recordsToProcess.length > 0) {
                    const recordsToInsert = [];
                    for (let i = recordsToProcess.length - 1; i >= 0; i--) {
                        const record = recordsToProcess[i];
                        let hasUnresolvedReferences = false;
                        // Resolve references if possible
                        for (const fieldName of Object.keys(record)) {
                            const fieldValue = record[fieldName];
                            if (typeof fieldValue === 'string' && fieldValue.startsWith('@')) {
                                if (idLookup[fieldValue]) {
                                    record[fieldName] = idLookup[fieldValue];
                                }
                                else {
                                    hasUnresolvedReferences = true;
                                }
                            }
                        }
                        // Only make the record 'ready' to insert if there are no unresolved references.
                        if (!hasUnresolvedReferences) {
                            recordsToInsert.push(record);
                            recordsToProcess.splice(i, 1);
                        }
                    }
                    if (recordsToInsert.length > 0) {
                        // Import the records (throws exception when the response has errors)
                        const treeResponse = await org.getConnection().request({
                            method: 'POST',
                            url: `${org.getConnection().baseUrl()}/composite/tree/${sobjectItem.sobject}`,
                            body: JSON.stringify({ records: recordsToInsert })
                        });
                        // Objects have been created, store references
                        output[sobjectItem.sobject].results = output[sobjectItem.sobject].results.concat(treeResponse.results);
                        output[sobjectItem.sobject].success += treeResponse.results.length;
                        treeResponse.results.forEach(result => {
                            idLookup[`@${result.referenceId}`] = result.id;
                        });
                    }
                    else if (recordsToProcess.length > 0) {
                        // If there are no records to insert, but there are still records to process, something went wrong.
                        throw new core_1.SfdxError(messages.getMessage('errorUnresolvedReferences', [sobjectItem.sobject]));
                    }
                }
            }
            // Record the progress (left pad the number of records)
            this.ux.log(messages.getMessage('statusProgress', [
                ('    ' + output[sobjectItem.sobject].success).slice(-5),
                sobjectItem.sobject
            ]));
        }
        const seconds = (Date.now() - startTime) / 1000;
        this.ux.stopSpinner(messages.getMessage('statusDone', [seconds]));
        // Return an object to be displayed with --json
        return output;
    }
    /**
     * Load records from a file
     *
     * @param sobjectItem the plan sobject item
     * @param plan the plan to import
     * @param fieldsByName map of field infos by field API name
     * @param metadata the plan metadata with the expected locale and timezone for CSV file parsing
     *
     * @return array of records
     */
    async loadRecords(sobjectItem, plan, fieldsByName, metadata) {
        const file = path.join(plan.input, `${sobjectItem.sobject}.${plan.format}`);
        if (plan.format === types_1.PlanFormat.JSON) {
            return await fs.readJSON(file);
        }
        if (plan.format === types_1.PlanFormat.CSV) {
            moment.locale(metadata.locale);
            const dateFormats = [
                moment.localeData().longDateFormat('l'),
                moment.localeData().longDateFormat('L'),
                moment.localeData().longDateFormat('ll'),
                moment.localeData().longDateFormat('LL'),
                moment.ISO_8601,
                moment.RFC_2822
            ];
            const timeFormats = [
                moment.localeData().longDateFormat('lll'),
                moment.localeData().longDateFormat('LLL')
            ];
            const dateTimeFormats = [
                moment.localeData().longDateFormat('lll'),
                moment.localeData().longDateFormat('LLL'),
                moment.localeData().longDateFormat('llll'),
                moment.localeData().longDateFormat('LLLL'),
                moment.ISO_8601,
                moment.RFC_2822
            ];
            return new Promise((resolve, reject) => {
                const records = [];
                parse_1.parseFile(file, {
                    objectMode: true,
                    headers: true,
                    trim: true,
                    ignoreEmpty: true
                }).on('data', row => {
                    const record = {
                        attributes: {
                            type: sobjectItem.sobject,
                            referenceId: row.referenceId,
                            recordTypeId: row.recordTypeId
                        }
                    };
                    Object.keys(row).forEach(fieldName => {
                        if (fieldsByName[fieldName] && fieldName !== 'Id' && fieldName !== 'RecordTypeId') {
                            let fieldValue = row[fieldName];
                            if (fieldValue === '') {
                                // Empty string represents null
                                fieldValue = null;
                            }
                            else {
                                // Parse Date, Time and Datetime in locale readable format
                                switch (fieldsByName[fieldName].type) {
                                    case 'date':
                                        fieldValue = moment.tz(fieldValue, dateFormats, false, metadata.timezone).format('YYYY-MM-DD');
                                        break;
                                    case 'time':
                                        const dateTimeValue = moment('2000-01-01').format('l') + ' ' + fieldValue;
                                        fieldValue = moment.tz(dateTimeValue, timeFormats, false, metadata.timezone).format('HH:mm:ss.SSS[Z]');
                                        break;
                                    case 'datetime':
                                        fieldValue = moment.tz(fieldValue, dateTimeFormats, false, metadata.timezone).toISOString();
                                        break;
                                    case 'boolean':
                                        fieldValue = fieldValue === 'true';
                                        break;
                                    case 'int':
                                    case 'double':
                                    case 'currency':
                                    case 'percent':
                                        fieldValue = Number(fieldValue);
                                        break;
                                }
                            }
                            record[fieldName] = fieldValue;
                        }
                    });
                    records.push(record);
                }).on('end', () => {
                    resolve(records);
                }).on('error', error => {
                    reject(error.message);
                });
            });
        }
    }
}
exports.default = Import;
// noinspection JSUnusedGlobalSymbols
Import.description = messages.getMessage('commandDescription');
// noinspection JSUnusedGlobalSymbols
Import.examples = [
    `$ sfdx sirenum:data:import my-plan.json`,
    `$ sfdx sirenum:data:import data/my-plan.json -d data/set1 -u my-org-alias`,
    `$ sfdx sirenum:data:import my-plan.json -i`
];
// noinspection JSUnusedGlobalSymbols
Import.args = [{ name: 'plan' }];
Import.flagsConfig = {
    plan: command_1.flags.filepath({ char: 'p', description: messages.getMessage('selectPlan') }),
    inputdir: command_1.flags.filepath({ char: 'd', description: messages.getMessage('inputFlagDescription') }),
    format: command_1.flags.enum({
        char: 'f',
        description: messages.getMessage('inputFlagFormat'),
        options: [types_1.PlanFormat.JSON, types_1.PlanFormat.CSV]
    }),
    align: command_1.flags.enum({
        char: 'a',
        description: messages.getMessage('inputFlagAlign'),
        options: [types_1.PlanAlignment.None, types_1.PlanAlignment.Week]
    })
};
Import.supportsUsername = true;
//# sourceMappingURL=import.js.map