import { flags } from '@salesforce/command';
import { AnyJson } from '@salesforce/ts-types';
import { InteractiveCommand } from "../../../shared/interactiveCommand";
import { PlanAlignment, PlanFormat } from "../../../shared/types";
/**
 * Import records into an org as listed in the plan
 */
export default class Import extends InteractiveCommand {
    static description: string;
    static examples: string[];
    static args: {
        name: string;
    }[];
    protected static flagsConfig: {
        plan: flags.Discriminated<flags.Option<string>>;
        inputdir: flags.Discriminated<flags.Option<string>>;
        format: flags.Discriminated<flags.Enum<PlanFormat>>;
        align: flags.Discriminated<flags.Enum<PlanAlignment>>;
    };
    protected static supportsUsername: boolean;
    private static getMaxRecordsPerCall;
    run(): Promise<AnyJson>;
    /**
     * Load records from a file
     *
     * @param sobjectItem the plan sobject item
     * @param plan the plan to import
     * @param fieldsByName map of field infos by field API name
     * @param metadata the plan metadata with the expected locale and timezone for CSV file parsing
     *
     * @return array of records
     */
    private loadRecords;
}
