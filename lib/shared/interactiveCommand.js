"use strict";
/*
 * Copyright Sirenum (c) 2020.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.InteractiveCommand = void 0;
const command_1 = require("@salesforce/command");
const core_1 = require("@salesforce/core");
const fs = require("fs-extra");
const path = require("path");
// Initialize Messages with the current plugin directory
core_1.Messages.importMessagesDirectory(__dirname);
// Load the specific messages for this file. Messages from @salesforce/command, @salesforce/core,
// or any library that is using the messages framework can also be loaded this way.
const messages = core_1.Messages.loadMessages('@sirenum/sfdx-plugin', 'interactiveCommand');
class InteractiveCommand extends command_1.SfdxCommand {
    /**
     * Override the flags to add the interactive option
     */
    static get flags() {
        const output = super.flags;
        // @ts-ignore - a type quirk
        output.interactive = command_1.flags.boolean({ char: 'i', description: messages.getMessage('interactiveFlagDescription') });
        return output;
    }
    /**
     * Prompt the user to select an org in interactive mode, or return the command org otherwise.
     * @param prompt The string that the user sees when prompted for selecting org
     * @return the selected org or undefined when no org was selected
     */
    async selectOrg(prompt) {
        const aliases = await core_1.Aliases.create(core_1.Aliases.getDefaultOptions());
        if (!this.flags.interactive) {
            // Not interactive mode - fix the alias and return the org
            if (this.org) {
                this.org.getConnection().getAuthInfoFields().alias =
                    (aliases.entries().find(alias => alias[1] === this.org.getUsername()) || [''])[0];
            }
            return this.org;
        }
        const orgs = [];
        let selectedNumber = null;
        for (const authItem of await core_1.AuthInfo.listAllAuthFiles()) {
            const username = path.basename(authItem, '.json');
            const authInfo = await core_1.AuthInfo.create({ username: username });
            // Add items with no expiration data or items that are not expired yet
            if (!authInfo.getFields().expirationDate || Date.now() < Date.parse(authInfo.getFields().expirationDate)) {
                // Find the alias
                if (!authInfo.getFields().alias) {
                    authInfo.getFields().alias = (aliases.entries().find(alias => alias[1] === authInfo.getUsername()) || [''])[0];
                }
                // Check if this is the selected org
                if (this.org && this.org.getUsername() === authInfo.getUsername()) {
                    selectedNumber = orgs.length + 1;
                }
                orgs.push({
                    authInfo: authInfo,
                    number: orgs.length + 1,
                    type: authInfo.getFields().isDevHub ? '(D)' : selectedNumber === orgs.length + 1 ? '(U)' : '',
                    alias: authInfo.getFields().alias,
                    username: authInfo.getFields().username,
                    orgId: authInfo.getFields().orgId,
                    expirationDate: authInfo.getFields().expirationDate
                });
            }
        }
        this.ux.log('').table(orgs, {
            columns: [
                { label: '#', key: 'number' },
                { label: '', key: 'type' },
                { label: messages.getMessage('orgAlias'), key: 'alias' },
                { label: messages.getMessage('orgUsername'), key: 'username' },
                { label: messages.getMessage('orgID'), key: 'orgId' },
                { label: messages.getMessage('orgExpirationDate'), key: 'expirationDate' }
            ]
        });
        const selection = Number(await this.ux.prompt(prompt, {
            default: selectedNumber,
            required: false
        }));
        if (selection > 0 && selection <= orgs.length) {
            return await core_1.Org.create({
                connection: await core_1.Connection.create({
                    authInfo: orgs[selection - 1].authInfo
                })
            });
        }
        else {
            // Invalid selection
            return undefined;
        }
    }
    /**
     * Prompt the user select a file with the specified extension in interactive mode, or return the file provided when
     * not in interactive mode.
     * @param prompt The string that the user sees when prompted for selecting file
     * @param ext The file extension to use, e.g. '.json'
     * @param file The default file to select
     * @return the selected file path or undefined if no file was selected
     */
    async selectFile(prompt, ext, file) {
        if (!this.flags.interactive) {
            // Not interactive mode - return the file as is
            return file;
        }
        let currentDir = path.dirname(file || '.');
        while (true) {
            // Show current path
            this.ux.log('').styledHeader(`${currentDir} (${path.resolve(currentDir)})`);
            const items = [];
            let defaultFileNumber = undefined;
            if (path.resolve(currentDir) !== path.resolve(path.join(currentDir, '../'))) {
                items.push({
                    number: items.length,
                    type: '../',
                    name: messages.getMessage('browseParent')
                });
            }
            fs.readdirSync(currentDir, { withFileTypes: true }).forEach(item => {
                if (item.isDirectory()) {
                    items.push({
                        number: items.length,
                        type: 'DIR',
                        name: item.name
                    });
                }
                if (item.isFile() && (!ext || ext === path.extname(item.name))) {
                    if (path.join(currentDir, item.name) === file) {
                        defaultFileNumber = items.length;
                    }
                    items.push({
                        number: items.length,
                        type: defaultFileNumber === items.length ? '-->' : '',
                        name: item.name
                    });
                }
            });
            this.ux.table(items, {
                columns: [
                    { label: '#', key: 'number' },
                    { label: '', key: 'type' },
                    { label: messages.getMessage('browseName'), key: 'name' }
                ]
            });
            const selection = Number(await this.ux.prompt(prompt, {
                default: defaultFileNumber,
                required: false
            }));
            const selectedItem = items.find(item => item.number === selection);
            if (selectedItem) {
                if (selectedItem.type === '../') {
                    // Up one level
                    currentDir = path.normalize(path.join(currentDir, '../'));
                }
                else if (selectedItem.type === 'DIR') {
                    // Open the directory
                    currentDir = path.normalize(path.join(currentDir, selectedItem.name));
                }
                else {
                    // A file was selected
                    return path.join(currentDir, selectedItem.name);
                }
            }
            else {
                // Invalid selection
                return undefined;
            }
        }
    }
    /**
     * Prompt the user select a directory in interactive mode, or return the directory provided when
     * not in interactive mode.
     * @param prompt The string that the user sees when prompted for selecting directory
     * @param dir The default directory to select
     * @return the selected directory path or undefined if no directory was selected
     */
    async selectFolder(prompt, dir) {
        if (!this.flags.interactive) {
            // Not interactive mode - return the directory as is
            return dir;
        }
        let currentDir = dir || '.';
        while (true) {
            // Show current path
            this.ux.log('').styledHeader(`${currentDir} (${path.resolve(currentDir)})`);
            const items = [];
            if (path.resolve(currentDir) !== path.resolve(path.join(currentDir, '../'))) {
                items.push({
                    number: 0,
                    type: '../',
                    name: messages.getMessage('browseParent')
                });
            }
            if (fs.pathExistsSync(currentDir)) {
                fs.readdirSync(currentDir, { withFileTypes: true }).forEach(item => {
                    if (item.isDirectory()) {
                        items.push({
                            number: items.length,
                            type: 'DIR',
                            name: item.name
                        });
                    }
                });
            }
            this.ux.table(items, {
                columns: [
                    { label: '#', key: 'number' },
                    { label: '', key: 'type' },
                    { label: messages.getMessage('browseName'), key: 'name' }
                ]
            });
            const selection = await this.ux.prompt(prompt, {
                default: currentDir,
                required: false
            });
            // The current directory was selected?
            if (selection.trim() === currentDir) {
                return currentDir;
            }
            if (!isNaN(Number(selection.trim()))) {
                // The user selected an item
                const selectedItem = items.find(item => item.number === Number(selection));
                if (selectedItem) {
                    if (selectedItem.type === '../') {
                        // Up one level
                        currentDir = path.normalize(path.join(currentDir, '../'));
                    }
                    else if (selectedItem.type === 'DIR') {
                        // Open the directory
                        currentDir = path.normalize(path.join(currentDir, selectedItem.name));
                    }
                }
                else {
                    // Invalid selection
                    return undefined;
                }
            }
            else {
                // The user typed a path
                currentDir = path.normalize(path.join(currentDir, selection.trim()));
            }
        }
    }
}
exports.InteractiveCommand = InteractiveCommand;
//# sourceMappingURL=interactiveCommand.js.map