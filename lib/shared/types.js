"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PlanAlignment = exports.PlanFormat = void 0;
var PlanFormat;
(function (PlanFormat) {
    PlanFormat["JSON"] = "json";
    PlanFormat["CSV"] = "csv";
})(PlanFormat = exports.PlanFormat || (exports.PlanFormat = {}));
var PlanAlignment;
(function (PlanAlignment) {
    PlanAlignment["None"] = "none";
    PlanAlignment["Week"] = "week";
})(PlanAlignment = exports.PlanAlignment || (exports.PlanAlignment = {}));
//# sourceMappingURL=types.js.map