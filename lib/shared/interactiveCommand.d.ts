import { flags, SfdxCommand } from "@salesforce/command";
import { Org } from "@salesforce/core";
export declare abstract class InteractiveCommand extends SfdxCommand {
    /**
     * Override the flags to add the interactive option
     */
    static get flags(): flags.Input<any>;
    /**
     * Prompt the user to select an org in interactive mode, or return the command org otherwise.
     * @param prompt The string that the user sees when prompted for selecting org
     * @return the selected org or undefined when no org was selected
     */
    protected selectOrg(prompt: string): Promise<Org>;
    /**
     * Prompt the user select a file with the specified extension in interactive mode, or return the file provided when
     * not in interactive mode.
     * @param prompt The string that the user sees when prompted for selecting file
     * @param ext The file extension to use, e.g. '.json'
     * @param file The default file to select
     * @return the selected file path or undefined if no file was selected
     */
    protected selectFile(prompt: string, ext: string, file?: string): Promise<string>;
    /**
     * Prompt the user select a directory in interactive mode, or return the directory provided when
     * not in interactive mode.
     * @param prompt The string that the user sees when prompted for selecting directory
     * @param dir The default directory to select
     * @return the selected directory path or undefined if no directory was selected
     */
    protected selectFolder(prompt: string, dir?: string): Promise<string>;
}
